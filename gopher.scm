(use posix utils tcp srfi-1 srfi-18)

;;; The most minimal usage of chicken-hole goes as follows:
;;;
;;; (include "./gopher.scm")
;;; (gopher-doc-root "<root directory>")
;;; (gopher-host-address "<domain name>")
;;; (gopher-start-server)
;;;

(define *gopher-port* 70)
(define *gopher-doc-root* "doc")
(define *gopher-bind-address* #f)
(define *gopher-host-address* "localhost")
(define *gopher-handlers* '())

(define (gopher-port p)
    (set! *gopher-port* p))

(define (gopher-doc-root str)
    (set! *gopher-doc-root* str))

(define (gopher-bind-address addr)
    (set! *gopher-bind-address* addr))

(define (gopher-host-address addr)
    (set! *gopher-host-address* addr))

(define (gopher-handlers handlers)
    (set! *gopher-handlers* handlers))

(define (add-gopher-handler handler)
    (set! *gopher-handlers* (cons handler *gopher-handlers*)))

(define (gopher-item-line type label #!optional (selector "/") (server *gopher-host-address*) (port 70))
    (string-append type label "\t" selector "\t" server "\t" (number->string port)))

(define (file->gopher-type filename)
    (let* ([dot-pos (if (string-index-right filename #\.) (string-index-right filename #\.) 0)]
           [fe (string-copy filename (+ 1 dot-pos) (string-length filename))])
        (cond [(equal? fe "gif") "g"]
              [(member fe '("png" "jpg" "jpeg" "bmp") string=?) "I"]
              [(member fe '("zip" "tar" "rar" "7z") string=?) "5"]
              [(member fe '("ps" "pdf" "doc" "docx") string=?) "d"]
              [(member fe '("mp3" "wav" "flac" "ogg") string=?) "s"]
              [(member fe '("mp4" "avi" "mkv" "wmv" "webm" "flv" "mov" "mpg" "mpeg") string=?) ";"]
              [(member fe '("bin" "exe") string=?) "9"]
              [(member fe '("html" "xhtml" "htm") string=?) "h"]
              [(member fe '("txt" "rtf" "md") string=?) "0"]
              [(member fe '("gopher" "goph" "gop" "g" "dir") string=?) "1"]
              [else "9"])))

(define (gopher-directory-printer dir out)
    (let ([dir (if (equal? dir "") "" dir)])
        (for-each (lambda (item)
                (thread-yield!)
                (write-line (gopher-item-line (if (directory? (string-append *gopher-doc-root* dir "/" item)) 
                                                  "1"
                                                  (file->gopher-type item))
                                              item
                                              (string-append dir "/" item)
                                              *gopher-host-address*
                                              *gopher-port*)
                            out))
                  (directory (string-append *gopher-doc-root* dir)))
        (write-line "." out)))

(define (illegal-selector? str)
    (any (lambda (s) (string=? s "..")) 
         (string-split str "/")))

(define (parse-selector selector)
    (if (string=? selector "")
        '("") 
        (string-split selector "\t")))

(define (absolute-selector selector)
    (string-append *gopher-doc-root* selector))

(define (gopher-send-file file out)
    (let* ([file-handle (file-open file open/rdonly)]
           [file-chunk (file-read file-handle 100)])
        (let loop ([file-handle file-handle]
                   [file-chunk file-chunk])
            (write-string (car file-chunk) #f out)
            (if (< (cadr file-chunk) 100)
	        (file-close file-handle)
                (begin (thread-yield!)
                       (loop file-handle (file-read file-handle 100)))))))

; The structure for user-made handlers is the same as the default one except they
; are expected to return #t if they handled the request completely and #f to pass 
; the request on to the next handler in line. Returning #t ends the handler chain 
; and closes the current socket. This allows for interesting configurations i.e.
; a handler higher on the chain outputting extra information for a file or
; directory and then passing control to the default handler to output the file's
; content or list the directory's files.
;
; The default handler will always return #t and thus should be the last handler in
; the handler list.
(define (gopher-default-handler selector out)
    (let* ([selector-path (car (parse-selector selector))]
           [selector-path (string-append (if (and (not (string=? selector-path ""))
                                                  (string=? (string-take selector-path 1) "/")) 
                                             "" "/")
                                         selector-path)]
           [full-selector (string-append *gopher-doc-root* selector-path)])
        (cond [(illegal-selector? full-selector)
                (write-line (gopher-item-line "3" "Permission denied.") out)]
              [(directory? full-selector)
                (if (member "gophermap" (directory full-selector) string=?)
                    (gopher-send-file (string-append full-selector "/gophermap") out)
                    (gopher-directory-printer selector-path out))]
              [(file-exists? full-selector) 
                (gopher-send-file full-selector out)]
              [else 
                (write-line (gopher-item-line "3" 
                                              (string-append "Could not find a file/directory named " 
                                                             selector-path)) 
                            out)])
        #t))

(define (gopher-print-connection-info-handler selector out) 
  (let ([time-string (seconds->string (current-seconds))]
	[parsed-selector (parse-selector selector)])
	  (let-values ([(local remote) (tcp-addresses out)])
		(print "[" time-string "] " remote " -> " (car parsed-selector) " " (if (null? (cdr parsed-selector)) "" (cadr parsed-selector)))
		#f)))

(add-gopher-handler gopher-default-handler)

;;; Uncomment the line below to enable printing connection info to stdout for each request
;(add-gopher-handler gopher-print-connection-info-handler)

(define (gopher-accept in out)
    (let loop ([selector (read-line in)]
          [handlers *gopher-handlers*])
        (unless (or (null? handlers)
                    ((car handlers) selector out))
                (loop selector (cdr handlers))))
        (close-output-port out))

(define (gopher-start-server)
    (let loop ([gopher-listener (tcp-listen *gopher-port* 100 *gopher-bind-address*)])
        (let-values ([(goph-in goph-out) (tcp-accept gopher-listener)])
            (thread-start! (lambda () 
                             (gopher-accept goph-in goph-out)))
            (loop gopher-listener))))

